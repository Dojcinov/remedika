angular.module('helloWorldApp')
    .factory('getJSON', function ($q,$timeout,$http) {


        var promise =
            {
                fetch : function (callback) {

                    return $timeout(function () {

                        return $http.get('./json/db.json').then(function (response) {

                            return response;
                        })
                    },30)
                }
            };

        return promise;
    });