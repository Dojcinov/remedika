/**
 * Created by vasko on 07/05/17.
 */
angular.module('helloWorldApp', [
    'ngRoute',
    'ngAnimate',
    'ds.clock'
])
.config
(
    ['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/',
            {
                templateUrl: 'views/home.html',
                controller: 'HomeCtrl'

            })

    }]
);

