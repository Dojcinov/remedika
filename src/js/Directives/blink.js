angular.module('helloWorldApp')
    .directive('blink', function($timeout) {

        return {
            restrict: 'E',
            transclude: true,

            controller: function($scope, $element) {
                var counter =0;
                function showElement() {

                    $element.css("display", "inline");

                    if(counter<10)
                    {
                        $timeout(hideElement, 1000);
                    }

                }

                function hideElement() {
                    $element.css("display", "none");
                    $timeout(showElement, 1000);
                    counter++;
                }

                showElement();

            },
            template: '<span ng-transclude=""></span>',
            replace: true
        };
    })