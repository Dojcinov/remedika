/**
 * Created by vasko on 07/05/17.
 */
const gulp = require('gulp');
const concat = require('gulp-concat');
var jsonConcat = require('gulp-json-concat');
const browserSync = require('browser-sync').create();
var convertEncoding = require('gulp-convert-encoding');
const scripts = require('./scripts');
const styles = require('./styles');
//var mergeJSON = require('./primer');
var devMode = false;







gulp.task('css', function () {
   gulp.src(styles)
       .pipe(concat('main.css'))
       .pipe(gulp.dest('./dist/css'))
       .pipe(browserSync.reload(
           {
               stream: true
           }
       ));
});

gulp.task('js', function () {
    gulp.src(scripts)
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('./dist/js'))
        .pipe(browserSync.reload(
            {
                stream: true
            }
        ));
});


gulp.task('html',function () {
    gulp.src('./src/templates/**/*.html')
        .pipe(gulp.dest('./dist/'))
        .pipe(browserSync.reload(
            {
                stream: true
            }
        ));
});
// gulp.task('json',function () {
//     return gulp.src('./primer.json')
//         // .pipe(convertEncoding({from: 'windows1251', to: 'utf8'}))
//         .pipe(jsonConcat('db.json',function(data){
//             return new Buffer(JSON.stringify(data));
//         }))
//         .pipe(gulp.dest('dist/json')).pipe(browserSync.reload(
//             {
//                 stream: true
//             }
//         ));
//
// });



gulp.task('build', function () {
   gulp.start(['css', 'js', 'html']);
});

gulp.task('browser-sync',function () {
    browserSync.init({

        server: {
           baseDir: 'dist'

        },
        port: 8080
    });
});

gulp.task('start', function () {
    devMode = true;
   gulp.start(['build', 'browser-sync']);
   gulp.watch(['./src/css/**/*.css'], ['css']);
    gulp.watch(['./src/js/**/*.js'], ['js']);
    gulp.watch(['./src/templates/**/*.html'], ['html']);
    // gulp.watch(['./**/*.json'], ['json']);
});
